# What is Liquid Democracy?

> Liquid democracy, also known as **delegative democracy** is a form of democracy 
 whereby an electorate has the option of vesting voting power in delegates 
 rather than voting directly themselves. 
> Liquid democracy is a broad category of either already-existing 
  or proposed popular-control apparatuses.  
> Voters can either vote directly or delegate their vote to other participants; 
  voters may also select different delegates for different issues.  
> In other words, individual A of a society can delegate their power 
  to another individual B – and withdraw such power again at any time.

# Existing ideas

 * http://www.brynosaurus.com/deleg/
 * https://fr.wikipedia.org/wiki/D%C3%A9mocratie_liquide

# [Specs](./specs/README.md)

# How can this project be used

This project's goal is to substitute, enhance or even replace the election procedure. Furthermore it should be used as a method of proposing laws and actions. This means that it will fall into the legislative and executive branch.

There will still be a governing body that takes the final decision on application of these laws and actions.

