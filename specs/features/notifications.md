# Notifications


Users should get notifications about different things

 - resolution status
 - new comments
 - delegates:
   - new votes
   - new drafts
   - new comments
 - possibly conflicting or unbalanced votes on tickets
   - tight votes (49/51)
   - the miniminum required amount of votes made the final vote

