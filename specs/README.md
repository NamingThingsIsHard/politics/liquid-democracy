# Voters

Votes can be anonymous or public. Public votes give the option to be followed by others, only if the public voter accepts delegate status.

## Direct voters
These don’t delegate the votes for a certain subjects

## Delegates

These accept to vote in place of another voter that has delegated their vote to the delegate. 

 - A delegate’s votes are public
 - A delegate my refuse their delegate status for certain subjects
   - E.g voter may be a delegate only on the subject of economics

  
# Ticket / Law

These are simply things that the general public can vote on

## Countdown

The time it will take until a ticket is really applied or reverted. Voters can change the opinions in this time.

## Elasticity 

The amount of time a ticket differing ticket resolution triggers a new vote.

**Example**

Dog tax has 2 options. Yes and no. The first time around yes wins and the dog tax is applied. After an X amount of time no becomes the majority opinion and stays that way for a Y amount of time, which triggers a new vote. Y is elasticity. 

**Possible solutions**

 - Function of votes necessary to reinit a vote that decreases over time


# Statements

Statements should have a confidence level depending on their reasoning. The reasoning should be checked by a language or software e.g https://coq.inria.fr/

# Compensation

Every delegate/person involved in the political debate shall be rewarded according to its involvement.

Problems: how to moderate this retribution process when overflooded with participant or when not enough people take part of it.

# Categorization

How will tickets be put into the right category automatically? Otherwise it could be possible to game the system by attributing the category with delegated most sympathetic to your cause.

