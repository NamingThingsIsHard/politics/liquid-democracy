# Fields

## Area

The area will determine how many people will be affected

## Elasticity

The amount of time required before the ticket can be voted upon again

## Type

**Free**

Can be binding or non-binding

**Official**

Are binding

**Government**

Binding and only votable by the government