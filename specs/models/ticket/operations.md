# Creation

Should everybody be able to create tickets?

# Resolution

Before a vote is final this is called **preresolution**.

Should be dependent on :

**how the delegate voices are counted**

 - blind preresolution
 - undecided preresoltion

**the area the ticket is affecting**

the more people it affects, the more people are needed in order for the resolution of the ticket

**the importance**

the more people it affects, the more people are needed in order for the resolution of the ticket

**Ticket**

For a specific ticket

