# Delegation

A user can delegate their voice/vote to another user. This can be done under the given conditions (which can be mixed). Some examples thereof are here

## Conditions

### Exclusive 

These conditions cannot be applied together

**No conditions (blind mode)**

Whatever the delegate votes for 

**Renewed delegation**

Any tickets will require approval for the vote to be delegated.


### Mixable

**Time limited**

You may have my voice for this given time-period / until date X.

**Topic**

You may select the topic 

