# Fields

## Willing to join government

This will make a user able to be in government.

# Relationships

## Delegates voices

Which voices a user has delegated to other users.

## Delegate of

Which voices have been delegated to this user
